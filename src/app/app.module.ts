import { AppComponent } from './app.component'
import { BrowserModule } from '@angular/platform-browser'
import { DetailsComponent } from './details/details.component'
import { EgresoComponent } from './egreso/egreso.component'
import { EgresoService } from './egreso/egreso.service'
import { FormComponent } from './form/form.component'
import { FormsModule } from '@angular/forms'
import { HeaderComponent } from './header/header.component'
import { IngresoComponent } from './ingreso/ingreso.component'
import { IngresoService } from './ingreso/ingreso.service'
import { NgModule } from '@angular/core'

@NgModule({
  declarations: [AppComponent, HeaderComponent, FormComponent, DetailsComponent, IngresoComponent, EgresoComponent],
  imports: [BrowserModule, FormsModule],
  providers: [IngresoService, EgresoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
