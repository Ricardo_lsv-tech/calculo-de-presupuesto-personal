import { Component } from '@angular/core'
import { Ingreso } from './ingreso.model'
import { IngresoService } from './ingreso.service'

@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.scss']
})
export class IngresoComponent {
  ingresos: Ingreso[] = []
  constructor(private ingresoService: IngresoService) {
    this.ingresos = this.ingresoService.ingreso
  }

  eliminarRegistro(ingreso: Ingreso) {
    this.ingresoService.eliniar(ingreso)
  }
}
