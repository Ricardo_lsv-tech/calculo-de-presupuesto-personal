import { Ingreso } from './ingreso.model'

export class IngresoService {
  ingreso: Ingreso[] = [new Ingreso('Salario', 1500000), new Ingreso('Venta Coche', 40000000)]

  eliniar(ingreso: Ingreso) {
    const index = this.ingreso.indexOf(ingreso)
    this.ingreso.splice(index, 1)
  }
}
