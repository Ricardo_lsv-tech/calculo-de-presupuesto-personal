import { Component } from '@angular/core'
import { Egreso } from '../egreso/egreso.model'
import { EgresoService } from '../egreso/egreso.service'
import { Ingreso } from '../ingreso/ingreso.model'
import { IngresoService } from '../ingreso/ingreso.service'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  description: string = ''
  valor: number = 0
  tipo: string = 'ingresoOperacion'

  constructor(private ingresoService: IngresoService, private egresoService: EgresoService) {}

  tipoOperacion(event: any) {
    this.tipo = event.target.value
  }

  agregarValor() {
    if (this.tipo === 'ingresoOperacion') {
      this.ingresoService.ingreso.push(new Ingreso(this.description, this.valor))
    } else if (this.tipo === 'egresoOperacion') {
      this.egresoService.egresos.push(new Egreso(this.description, this.valor))
    }
  }
}
