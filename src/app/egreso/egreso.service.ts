import { Egreso } from './egreso.model'
import { Ingreso } from '../ingreso/ingreso.model'

export class EgresoService {
  egresos: Egreso[] = [new Egreso('Renta Apto', 350000), new Egreso('Ropa', 1500000)]

  eliniar(egreso: Ingreso) {
    const index = this.egresos.indexOf(egreso)
    this.egresos.splice(index, 1)
  }
}
