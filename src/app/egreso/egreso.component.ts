import { Component, Input } from '@angular/core'

import { Egreso } from './egreso.model'
import { EgresoService } from './egreso.service'

@Component({
  selector: 'app-egreso',
  templateUrl: './egreso.component.html',
  styleUrls: ['./egreso.component.scss']
})
export class EgresoComponent {
  egresos: Egreso[] = []
  @Input() ingresoTotal: number = 0
  constructor(private egresoService: EgresoService) {
    this.egresos = this.egresoService.egresos
  }

  calcularPorcentaje(egreso: Egreso) {
    return egreso.valor / this.ingresoTotal
  }

  eliminarRegistro(egreso: Egreso) {
    this.egresoService.eliniar(egreso)
  }
}
