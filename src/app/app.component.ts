import { Component } from '@angular/core'
import { Egreso } from './egreso/egreso.model'
import { EgresoService } from './egreso/egreso.service'
import { Ingreso } from './ingreso/ingreso.model'
import { IngresoService } from './ingreso/ingreso.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PRESUPUESTO'

  ingresos: Ingreso[] = []
  egresos: Egreso[] = []

  constructor(private ingresoService: IngresoService, private egresoService: EgresoService) {
    this.ingresos = ingresoService.ingreso
    this.egresos = egresoService.egresos
  }

  getIngresoTotal() {
    let ingresoTotal = 0
    this.ingresos.forEach((ingreso) => {
      ingresoTotal += Number(ingreso.valor)
    })
    return ingresoTotal
  }

  getEgresoTotal() {
    let egresoTotal = 0
    this.egresos.forEach((egreso) => {
      egresoTotal += Number(egreso.valor)
    })
    return egresoTotal
  }

  getPorcentajeTotal() {
    return this.getEgresoTotal() / this.getIngresoTotal()
  }

  getPresupuestoTotal() {
    return this.getIngresoTotal() - this.getEgresoTotal()
  }
}
