import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() title!: string
  @Input() ingresoTotal: number
  @Input() egresoTotal: number
  @Input() porcentajeTotal: number
  @Input() presupuestoTotal: number
}
